#!/bin/bash

#
#
#
sudo apt-get install -y git
cd ~/

mkdir -p devops
chmod 755 devops
cd devops

rm -rf provision_script

git clone https://sajeern@bitbucket.org/qburst/provision_script.git
cd provision_script

git pull --rebase

source provision.sh $1 "$2" $3 $4
