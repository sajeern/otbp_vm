# Drupal Vagrant by Sajeer

This document guide you how to setup vagrant for Drupal project.

## Requirements Satisfied
PHP 7
MySQL 5.6.31
Apache 2.4.7

## How to setup
1. Clone or Download this repository(git@bitbucket.org:qburst/drupal_vagrant.git) in your local machine
2. Go to the repository folder
3. Please add your keys into the remote repo (Github / Bitbucket / Codebase)
4. Go to {repo}/src/ folder and make a copy for default.config.yaml and create a file config.yaml in that folder
5. Change the values in config.yaml 
6. Type the following command
   *source setup.sh*
      OR
   *./setup.sh*

7. Once everything is success, type *vagrant halt* to close the VM then *vagrant up* to start the machine
8. To enter into the Host machine type *vagrant ssh* after going inside the repo directory

