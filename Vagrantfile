
# Minimum Vagrant version
Vagrant.require_version ">= 1.6.0"

# Minimum Vagrant API version
VAGRANT_API_VERSION = "2"

# Require YAML module
require 'yaml'

confDir = $confDir ||= File.expand_path("src", File.dirname(__FILE__))

provisionScript = confDir + '/provision.sh'

boxConfig = YAML::load_file(confDir + '/config.yaml')

Vagrant.configure(VAGRANT_API_VERSION) do |config|
    config.vm.box = boxConfig['vbox_name']

    config.vm.provider :virtualbox do |vb|
        vb.name = boxConfig['machine_name']
    end

    config.vm.provider boxConfig['provider'] do |vb|
        vb.gui = false
        vb.memory = boxConfig['memory']
        vb.cpus = boxConfig['cpus']
    end

    if File.exists? provisionScript then
    	config.vm.provision "shell" do |s|
            s.path = provisionScript
            s.args = [boxConfig['host_name'], boxConfig['user_short_name'], boxConfig['user_email'], boxConfig['codebase_git_url']]
    	end
    end
    config.vm.network "forwarded_port", guest: 80, host: 8080, auto_correct: true
end
