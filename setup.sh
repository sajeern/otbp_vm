#!/bin/bash


# sudo su - -c "echo deb http://download.virtualbox.org/virtualbox/debian xenial contrib >> /etc/apt/sources.list"

# # Add keys
# wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
# wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
# sudo apt-get update
# sudo apt-get install virtualbox-5.1


sudo apt-get install -y vagrant
sudo apt-get install -y --force-yes virtualbox virtualbox-dkms linux-headers-generic
sudo apt-get install dkms
sudo apt-get install virtualbox-guest-additions-iso
vagrant plugin install vagrant-vbguest

sudo dpkg-reconfigure virtualbox-dkms
sudo dpkg-reconfigure virtualbox
sudo modprobe vboxdrv

sudo apt-get -y autoremove

mkdir -p src/keys
cp ~/.ssh/* src/keys/

export BOX_NAME="Ubuntu14.04"
export BOX_EXISTS=$(vagrant box list | grep "$BOX_NAME")

if [[ "$BOX_EXISTS" == "" ]]; then
  vagrant box add Ubuntu14.04 http://10.9.2.139:9050/ubuntu-14.04-amd64.box
fi

vagrant up --provision

